var config = require('../config');
var express = require('express');
var router = express.Router();
const rateLimit = require("express-rate-limit");
const responseTime = require('response-time')
const axios = require('axios');
//const redis = require('redis');
const redis = require("async-redis");
var Request = require("request");
let datetime = require('node-datetime');
const pool=require('../mysqlpool.js');//using connection pooling here
const client = redis.createClient({host: '5.152.223.250', port: 6379, auth_pass: 'Zaq1@wsx'});

module.exports={
dummypage:(req,res)=>{
res.render("index")
},

listmarketbookcontroller:(req,res)=>{
    //console.log(req.body);
    let sqltoken="select accesstoken,refreshdate from betfairaccesstokennew where entrydate=? ";

async function query()
{

  let dt = datetime.create();
  let today = dt.format('Y-m-d H:M:S'); 
 // await client.set("string key", "string val");
 const sessiontoken = await client.get(config.rediskey);
 const refreshdate = await client.get(config.redisdate);

  let firstDate = new Date(today),
    secondDate = new Date(refreshdate),
    timeDifference = Math.abs(secondDate.getTime() - firstDate.getTime())/ 36e5;
    if(timeDifference<8 && typeof sessiontoken=="string")
    {
      //console.log(timeDifference);
        ////////Get token from redis database
  
        ///Update the tokens in redis database as well
        
        
          
          let data=req.body.marketIds
          console.log(req.body);
          //console.log(req.body);
          Request.post({
            "headers":{ 'X-Application' : config.key, 'X-Authentication' : sessiontoken ,'content-type' : 'application/json' },
            "url": config.listmarketbook,
            "body": JSON.stringify([{
              "jsonrpc": "2.0",
              "method": "SportsAPING/v1.0/listMarketBook",
              "params": {
                  "marketIds": [...data],
                  "priceProjection": {
                      "priceData": ["EX_BEST_OFFERS"],
                      "virtualise": "true"
                  }
              },
              "id": 1
          }])
        }, (error, response, body) => {
            if(error) {
                return console.dir(error);
            }
            else{
              //console.log(body);
              //client.setex(data[0], 3600, JSON.stringify({ source: 'Redis Cache', ...responseJSON, }));
              let result=JSON.parse(body)[0].result;
             // client.setex(data[0], 3600000000,JSON.stringify(result));
              /*for(let i=0;i<result.length;i++)
              {
               client.setex(result[i].marketId, 36000000000000000,JSON.stringify(result[i]));
              }*/
              res.json({
                result
              })
            }
            
        });
      


    }

    else if(timeDifference>=8 || typeof sessiontoken=="undefined"){
       
const sqltokenresult=JSON.parse(JSON.stringify(await pool.query(sqltoken,[config.columnname]))) ;//getting the userbalance of each user entry (for particular marketid)
  
///Update the tokens in redis database as well


  //console.log(sqltokenresult);
  await client.set(config.rediskey, sqltokenresult[0].accesstoken);
  await client.set(config.redisdate, sqltokenresult[0].refreshdate);
  let data=req.body.marketIds
  //console.log(data[0]);
  Request.post({
    "headers":{ 'X-Application' : config.key, 'X-Authentication' : sqltokenresult[0].accesstoken ,'content-type' : 'application/json' },
    "url": config.listmarketbook,
    "body": JSON.stringify([{
      "jsonrpc": "2.0",
      "method": "SportsAPING/v1.0/listMarketBook",
      "params": {
          "marketIds": [...data],
          "priceProjection": {
              "priceData": ["EX_BEST_OFFERS"],
              "virtualise": "true"
          }
      },
      "id": 1
  }])
}, (error, response, body) => {
    if(error) {
        return console.dir(error);
    }
    else{
      //console.log(body);
      //client.setex(data[0], 3600, JSON.stringify({ source: 'Redis Cache', ...responseJSON, }));
      let result=JSON.parse(body)[0].result;
      //client.setex(data[0], 3600000000,JSON.stringify(result));
     
        /*sfor(let i=0;i<result.length;i++)
              {
               client.setex(result[i].marketId, 36000000000000000,JSON.stringify(result[i]));
              }*/

      res.json({
        result
      })
    }
    
});

    }
 


}



query();
 

},

// list eventtypes method

listeventtypes:(req,res)=>{
  
    //console.log("inside list eventtypes");

    //console.log(req.body);
    let sqltoken="select accesstoken,refreshdate from betfairaccesstokennew where entrydate=? ";

async function query()
{

  let dt = datetime.create();
  let today = dt.format('Y-m-d H:M:S'); 
 // await client.set("string key", "string val");

 const sessiontoken = await client.get(config.rediskey);
 const refreshdate = await client.get(config.redisdate);
 //console.log(typeof sessiontoken);
  let firstDate = new Date(today),
    secondDate = new Date(refreshdate),
    timeDifference = Math.abs(secondDate.getTime() - firstDate.getTime())/ 36e5;
    if(timeDifference<8 && typeof sessiontoken=="string")
    {
      //console.log("Time difference",timeDifference);
        ////////Get token from redis database
  
        ///Update the tokens in redis database as well
        
        
          
         
          //console.log(req.body);
          Request.post({
            "headers":{ 'X-Application' : config.key, 'X-Authentication' : sessiontoken ,'content-type' : 'application/json','accept':'application/json' },
    
            "url": config.listeventtypes,
            "body": JSON.stringify({
                "filter" : { }
            })
        }, (error, response, body) => {
            if(error) {
                return console.dir(error);
            }
            else{
              //console.log(body);
              let result=JSON.parse(body);
              //console.log("finalresult",result);
              res.json(result);
            }
            
        });
      


    }

    else if(timeDifference>=8 || typeof sessiontoken=="undefined"){
       
const sqltokenresult=JSON.parse(JSON.stringify(await pool.query(sqltoken,[config.columnname]))) ;//getting the userbalance of each user entry (for particular marketid)
  
///Update the tokens in redis database as well


  //console.log(sqltokenresult);
  await client.set(config.rediskey, sqltokenresult[0].accesstoken);
  await client.set(config.redisdate, sqltokenresult[0].refreshdate);
  let data=req.body.marketIds
  //console.log(data[0]);
  Request.post({
    "headers":{ 'X-Application' : config.key, 'X-Authentication' : sqltokenresult[0].accesstoken ,'content-type' : 'application/json','accept':'application/json' },

    "url": config.listeventtypes,
    "body": JSON.stringify({
        "filter" : { }
    })
}, (error, response, body) => {
    if(error) {
        return console.dir(error);
    }
    else{
      //console.log(body);
      let result=JSON.parse(body);
    

      res.json({
        result
      })
    }
    
});

    }
 


}



query();



},


// send eventtypeids to get the response;
listeventsbyeventtype:(req,res)=>{
    //console.log("inside list eventtypes");

    //console.log(req.body);
    let sqltoken="select accesstoken,refreshdate from betfairaccesstokennew where entrydate=? ";
  let data=req.body.eventTypeIds;
  let fromdate=req.body.from;
  let todate=req.body.to;
async function query()
{

  let dt = datetime.create();
  let today = dt.format('Y-m-d H:M:S'); 
 // await client.set("string key", "string val");
 
const sessiontoken = await client.get(config.rediskey);
const refreshdate = await client.get(config.redisdate);

  let firstDate = new Date(today),
    secondDate = new Date(refreshdate),
    timeDifference = Math.abs(secondDate.getTime() - firstDate.getTime())/ 36e5;
    if(timeDifference<8 && typeof sessiontoken=="string")
    {
      //console.log("Time difference",timeDifference);
        ////////Get token from redis database
  
        ///Update the tokens in redis database as well
        
        
          
         
          //console.log(req.body);
          Request.post({
            "headers":{ 'X-Application' : config.key, 'X-Authentication' : sessiontoken ,'content-type' : 'application/json','Accept':'application/json' },
            "url": config.listevents,
            "body": JSON.stringify({"filter":{
                "eventTypeIds": [...data],
                "marketStartTime": {
                                   "from": fromdate,
                                   "to": todate
                               }
               }
               
               })
        }, (error, response, body) => {
            if(error) {
                return console.dir(error);
            }
            else{
              //console.log(body);
              let result=JSON.parse(body);
              //console.log("finalresult",result);
              res.json(result);
            }
            
        });
      


    }

    else if(timeDifference>=8 || typeof sessiontoken=="undefined"){
       
const sqltokenresult=JSON.parse(JSON.stringify(await pool.query(sqltoken,[config.columnname]))) ;//getting the userbalance of each user entry (for particular marketid)
  
///Update the tokens in redis database as well


  //console.log(sqltokenresult);
  await client.set(config.rediskey, sqltokenresult[0].accesstoken);
  await client.set(config.redisdate, sqltokenresult[0].refreshdate);
  let data=req.body.marketIds
  //console.log(data[0]);
  Request.post({
    "headers":{ 'X-Application' : config.key, 'X-Authentication' : sqltokenresult[0].accesstoken ,'content-type' : 'application/json','Accept':'application/json' },
    "url": config.listevents,
    "body": JSON.stringify({"filter":{
        "eventTypeIds": [...data],
        "marketStartTime": {
                           "from": fromdate,
                           "to": todate
                       }
       }
       
       })
}, (error, response, body) => {
    if(error) {
        return console.dir(error);
    }
    else{
      //console.log(body);
      let result=JSON.parse(body);
    

      res.json({
        result
      })
    }
    
});

    }
 


}



query();


},
listeventsbyeventtypejava:(req,res)=>{
    //console.log("inside list eventtypes");

    //console.log(req.body);
    let sqltoken="select accesstoken,refreshdate from betfairaccesstokennew where entrydate=? ";
  let data=req.body.eventTypeIds;
  let fromdate=req.body.from;
  let todate=req.body.to;
async function query()
{

  let dt = datetime.create();
  let today = dt.format('Y-m-d H:M:S'); 
 // await client.set("string key", "string val");
 
const sessiontoken = await client.get(config.rediskey);
const refreshdate = await client.get(config.redisdate);

  let firstDate = new Date(today),
    secondDate = new Date(refreshdate),
    timeDifference = Math.abs(secondDate.getTime() - firstDate.getTime())/ 36e5;
    if(timeDifference<8 && typeof sessiontoken=="string")
    {
      //console.log("Time difference",timeDifference);
        ////////Get token from redis database
  
        ///Update the tokens in redis database as well
        
        
          
         
          //console.log(req.body);
          Request.post({
            "headers":{ 'X-Application' : config.key, 'X-Authentication' : sessiontoken ,'content-type' : 'application/json','Accept':'application/json' },
            "url": config.listevents,
            "body": JSON.stringify({"filter":{
                "eventTypeIds": [...data],
                "marketStartTime": {
                                   "from": fromdate,
                                   "to": todate
                               }
               }
               
               })
        }, (error, response, body) => {
            if(error) {
                return console.dir(error);
            }
            else{
              //console.log(body);
              let result=JSON.parse(body);
              console.log(result);
              
              //console.log("finalresult",result);
              res.json(result);
            }
            
        });
      


    }

    else if(timeDifference>=8 || typeof sessiontoken=="undefined"){
       
const sqltokenresult=JSON.parse(JSON.stringify(await pool.query(sqltoken,[config.columnname]))) ;//getting the userbalance of each user entry (for particular marketid)
  
///Update the tokens in redis database as well


  //console.log(sqltokenresult);
  await client.set(config.rediskey, sqltokenresult[0].accesstoken);
  await client.set(config.redisdate, sqltokenresult[0].refreshdate);
  let data=req.body.marketIds
  //console.log(data[0]);
  Request.post({
    "headers":{ 'X-Application' : config.key, 'X-Authentication' : sqltokenresult[0].accesstoken ,'content-type' : 'application/json','Accept':'application/json' },
    "url": config.listevents,
    "body": JSON.stringify({"filter":{
        "eventTypeIds": [...data],
        "marketStartTime": {
                           "from": fromdate,
                           "to": todate
                       }
       }
       
       })
}, (error, response, body) => {
    if(error) {
        return console.dir(error);
    }
    else{
      //console.log(body);
      let result=JSON.parse(body);
    
      let finalresult=result.result;
      //console.log("finalresult",result);
      res.json(result);
    }
    
});

    }
 


}



query();


},

// send competitionids here for the request

listeventsbycompetitionid:(req,res)=>{
    //console.log("inside list eventtypes");

    //console.log(req.body);
    let sqltoken="select accesstoken,refreshdate from betfairaccesstokennew where entrydate=? ";
  let data=req.body.competitionIds;
  let fromdate=req.body.from;
  let todate=req.body.to;
async function query()
{

  let dt = datetime.create();
  let today = dt.format('Y-m-d H:M:S'); 
 // await client.set("string key", "string val");
 const sessiontoken = await client.get(config.rediskey);
 const refreshdate = await client.get(config.redisdate);

  let firstDate = new Date(today),
    secondDate = new Date(refreshdate),
    timeDifference = Math.abs(secondDate.getTime() - firstDate.getTime())/ 36e5;
    if(timeDifference<8 && typeof sessiontoken=="string")
    {
      //console.log("Time difference",timeDifference);
        ////////Get token from redis database
  
        ///Update the tokens in redis database as well
        
        
          
         
          //console.log(req.body);
          Request.post({
            "headers":{ 'X-Application' : config.key, 'X-Authentication' : sessiontoken ,'content-type' : 'application/json','Accept':'application/json' },
            "url": config.listevents,
            "body": JSON.stringify({"filter":{
                "competitionIds": [...data],
                "marketStartTime": {
                                   "from": fromdate,
                                   "to": todate
                               }
               }
               
               })
        }, (error, response, body) => {
            if(error) {
                return console.dir(error);
            }
            else{
              //console.log(body);
              let result=JSON.parse(body);
              //console.log("finalresult",result);
              res.json(result);
            }
            
        });
      


    }

    else if(timeDifference>=8 || typeof sessiontoken=="undefined"){
       
const sqltokenresult=JSON.parse(JSON.stringify(await pool.query(sqltoken,[config.columnname]))) ;//getting the userbalance of each user entry (for particular marketid)
  
///Update the tokens in redis database as well


  //console.log(sqltokenresult);
  await client.set(config.rediskey, sqltokenresult[0].accesstoken);
  await client.set(config.redisdate, sqltokenresult[0].refreshdate);
  let data=req.body.marketIds
  //console.log(data[0]);
  Request.post({
    "headers":{ 'X-Application' : config.key, 'X-Authentication' : sqltokenresult[0].accesstoken ,'content-type' : 'application/json','Accept':'application/json' },
    "url": config.listevents,
    "body": JSON.stringify({"filter":{
        "competitionIds": [...data],
        "marketStartTime": {
                           "from": fromdate,
                           "to": todate
                       }
       }
       
       })
}, (error, response, body) => {
    if(error) {
        return console.dir(error);
    }
    else{
      //console.log(body);
      let result=JSON.parse(body);
    

      res.json(result);
    }
    
});

    }
 


}



query();


},

listmarketcataloguejava:(req,res)=>{
    //console.log("inside list eventtypes");

    //console.log(req.body);
    let sqltoken="select accesstoken,refreshdate from betfairaccesstokennew where entrydate=? ";
  let data=req.body.eventids;

async function query()
{

  let dt = datetime.create();
  let today = dt.format('Y-m-d H:M:S'); 
 // await client.set("string key", "string val");
 const sessiontoken = await client.get(config.rediskey);
 const refreshdate = await client.get(config.redisdate);

  let firstDate = new Date(today),
    secondDate = new Date(refreshdate),
    timeDifference = Math.abs(secondDate.getTime() - firstDate.getTime())/ 36e5;
    if(timeDifference<8 && typeof sessiontoken=="string")
    {
      //console.log("Time difference",timeDifference);
        ////////Get token from redis database
  
        ///Update the tokens in redis database as well
        
        
          
         
          //console.log(req.body);
          Request.post({
            "headers":{ 'X-Application' : config.key, 'X-Authentication' : sessiontoken ,'content-type' : 'application/json','Accept':'application/json' },
            "url": config.listmarketcatalogues,
            "body": JSON.stringify({
                "filter": {
                    "eventIds": [
                       ...data
                    ]
                },
                "maxResults": "200",
                "marketProjection": [
                    "COMPETITION",
                    "EVENT",
                    "EVENT_TYPE",
                    "RUNNER_DESCRIPTION",
                    "RUNNER_METADATA",
                    "MARKET_START_TIME"
                ]
            })
        }, (error, response, body) => {
            if(error) {
                return console.dir(error);
            }
            else{
              //console.log(body);
              let result=JSON.parse(body);
              console.log(result);
              //console.log("finalresult",result);
              res.json(result)
            }
            
        });
      


    }

    else if(timeDifference>=8 || typeof sessiontoken=="undefined"){
       
const sqltokenresult=JSON.parse(JSON.stringify(await pool.query(sqltoken,[config.columnname]))) ;//getting the userbalance of each user entry (for particular marketid)
  
///Update the tokens in redis database as well


  //console.log(sqltokenresult);
  await client.set(config.rediskey, sqltokenresult[0].accesstoken);
  await client.set(config.redisdate, sqltokenresult[0].refreshdate);
  let data=req.body.marketIds
  //console.log(data[0]);
  Request.post({
    "headers":{ 'X-Application' : config.key, 'X-Authentication' : sqltokenresult[0].accesstoken ,'content-type' : 'application/json','Accept':'application/json' },
    "url": config.listmarketcatalogues,
    "body": JSON.stringify({
        "filter": {
            "eventIds": [
               ...data
            ]
        },
        "maxResults": "200",
        "marketProjection": [
            "COMPETITION",
            "EVENT",
            "EVENT_TYPE",
            "RUNNER_DESCRIPTION",
            "RUNNER_METADATA",
            "MARKET_START_TIME"
        ]
    })
}, (error, response, body) => {
    if(error) {
        return console.dir(error);
    }
    else{
      //console.log(body);
      let result=JSON.parse(body);
      console.log(result);

      res.json(result)
    }
    
});

    }
 


}



query();


},

// listmarketcatalogue

listmarketcatalogue:(req,res)=>{
    //console.log("inside list eventtypes");

    //console.log(req.body);
    let sqltoken="select accesstoken,refreshdate from betfairaccesstokennew where entrydate=? ";
  let data=req.body.eventids;

async function query()
{

  let dt = datetime.create();
  let today = dt.format('Y-m-d H:M:S'); 
 // await client.set("string key", "string val");
 const sessiontoken = await client.get(config.rediskey);
 const refreshdate = await client.get(config.redisdate);

  let firstDate = new Date(today),
    secondDate = new Date(refreshdate),
    timeDifference = Math.abs(secondDate.getTime() - firstDate.getTime())/ 36e5;
    if(timeDifference<8 && typeof sessiontoken=="string")
    {
      //console.log("Time difference",timeDifference);
        ////////Get token from redis database
  
        ///Update the tokens in redis database as well
        
        
          
         
          //console.log(req.body);
          Request.post({
            "headers":{ 'X-Application' : config.key, 'X-Authentication' : sessiontoken ,'content-type' : 'application/json','Accept':'application/json' },
            "url": config.listmarketcatalogues,
            "body": JSON.stringify({
                "filter": {
                    "eventIds": [
                       ...data
                    ]
                },
                "maxResults": "200",
                "marketProjection": [
                    "COMPETITION",
                    "EVENT",
                    "EVENT_TYPE",
                    "RUNNER_DESCRIPTION",
                    "RUNNER_METADATA",
                    "MARKET_START_TIME"
                ]
            })
        }, (error, response, body) => {
            if(error) {
                return console.dir(error);
            }
            else{
              //console.log(body);
              let result=JSON.parse(body);
              //console.log("finalresult",result);
              res.json(result);
            }
            
        });
      


    }

    else if(timeDifference>=8 || typeof sessiontoken=="undefined"){
       
const sqltokenresult=JSON.parse(JSON.stringify(await pool.query(sqltoken,[config.columnname]))) ;//getting the userbalance of each user entry (for particular marketid)
  
///Update the tokens in redis database as well


  //console.log(sqltokenresult);
  await client.set(config.rediskey, sqltokenresult[0].accesstoken);
  await client.set(config.redisdate, sqltokenresult[0].refreshdate);
  let data=req.body.marketIds
  //console.log(data[0]);
  Request.post({
    "headers":{ 'X-Application' : config.key, 'X-Authentication' : sqltokenresult[0].accesstoken ,'content-type' : 'application/json','Accept':'application/json' },
    "url": config.listmarketcatalogues,
    "body": JSON.stringify({
        "filter": {
            "eventIds": [
               ...data
            ]
        },
        "maxResults": "200",
        "marketProjection": [
            "COMPETITION",
            "EVENT",
            "EVENT_TYPE",
            "RUNNER_DESCRIPTION",
            "RUNNER_METADATA",
            "MARKET_START_TIME"
        ]
    })
}, (error, response, body) => {
    if(error) {
        return console.dir(error);
    }
    else{
      //console.log(body);
      let result=JSON.parse(body);
    

      res.json(result);
    }
    
});

    }
 


}



query();


},
// gettingcompetitions from eventtype

// send eventtypeids to get the response;
listcompetitions:(req,res)=>{
    //console.log("inside list competitions");

    //console.log(req.body);
    let sqltoken="select accesstoken,refreshdate from betfairaccesstokennew where entrydate=? ";
  let data=req.body.eventtypeids;

async function query()
{
try{


  let dt = datetime.create();
  let today = dt.format('Y-m-d H:M:S'); 
 // await client.set("string key", "string val");
 const sessiontoken = await client.get(config.rediskey);
 const refreshdate = await client.get(config.redisdate);

  let firstDate = new Date(today),
    secondDate = new Date(refreshdate),
    timeDifference = Math.abs(secondDate.getTime() - firstDate.getTime())/ 36e5;
    if(timeDifference<8 && typeof sessiontoken=="string")
    {
      //console.log("Time difference",timeDifference);
        ////////Get token from redis database
  
        ///Update the tokens in redis database as well
        
        
          
         
          //console.log(req.body);
          Request.post({
            "headers":{ 'X-Application' : config.key, 'X-Authentication' : sessiontoken ,'content-type' : 'application/json','Accept':'application/json' },
            "url": config.listcompetion,
            "body": JSON.stringify({
                "filter": {
                       "eventTypeIds": [...data]
                    }
                 
              })
        }, (error, response, body) => {
            if(error) {
                return console.dir(error);
            }
            else{
              //console.log(body);
              let result=JSON.parse(body);
              //console.log("finalresult",result);
              res.json(result);
            }
            
        });
      


    }

    else if(timeDifference>=8 || typeof sessiontoken=="undefined"){
       
const sqltokenresult=JSON.parse(JSON.stringify(await pool.query(sqltoken,[config.columnname]))) ;//getting the userbalance of each user entry (for particular marketid)
  
///Update the tokens in redis database as well


  //console.log(sqltokenresult);
  await client.set(config.rediskey, sqltokenresult[0].accesstoken);
  await client.set(config.redisdate, sqltokenresult[0].refreshdate);
  let data=req.body.marketIds
  //console.log(data[0]);
  Request.post({
    "headers":{ 'X-Application' : config.key, 'X-Authentication' : sqltokenresult[0].accesstoken ,'content-type' : 'application/json','Accept':'application/json' },
    "url": config.listcompetion,
    "body": JSON.stringify({
        "filter": {
               "eventTypeIds": [...data]
            }
         
      })
}, (error, response, body) => {
    if(error) {
        return console.dir(error);
    }
    else{
      //console.log(body);
      let result=JSON.parse(body);
    

      res.json(result);
    }
    
});

    }
 

  } ///try

  catch(ex){
    //console.log(ex);

  }

}



query();


},


}